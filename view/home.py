from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse


def bindWebApplication(app: FastAPI):
    template = Jinja2Templates(directory="template")

    @app.get("/", response_class=HTMLResponse)
    async def login(request: Request):
        context = dict(request=request)
        return template.TemplateResponse("login.html", context)
