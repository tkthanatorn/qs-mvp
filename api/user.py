from fastapi import APIRouter
from infrastructure import prisma
from adapter.repo import UserRepo
from core.service import UserService
from adapter.handler import UserHandler


def getUserRouter() -> APIRouter:
    router = APIRouter(prefix="/api/user/login")

    repo = UserRepo(prisma)
    serv = UserService(repo)
    hdl = UserHandler(serv)

    hdl.bindUserAPI(router)
    return router
