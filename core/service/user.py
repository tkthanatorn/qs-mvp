from core.domain import IUserService
from core.port import IUserRepo


class UserService(IUserService):
    repo: IUserRepo

    def __init__(self, repo: IUserRepo) -> None:
        super().__init__()
        self.repo = repo

    async def register():
        pass

    async def login():
        pass

    async def getByEmail():
        pass
