
class IUserService:
    async def register():
        raise NotImplementedError

    async def login():
        raise NotImplementedError

    async def getByEmail():
        raise NotImplementedError
    