from prisma.client import models


class IUserRepo:
    async def create(user: models.User):
        raise NotImplementedError

    async def getByEmail(email: str) -> models.User | None:
        raise NotImplementedError
