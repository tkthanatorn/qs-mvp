import os

MODE = str(os.environ.get("MODE"))
LOG_LEVEL = int(os.environ.get("LOG_LEVEL"))
APP_HOST = str(os.environ.get("APP_HOST"))
APP_PORT = int(os.environ.get("APP_PORT"))
