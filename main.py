from dotenv import load_dotenv
import infrastructure
import os
import logging
import uvicorn
from config import setting
from fastapi import FastAPI
from fastapi.middleware.wsgi import WSGIMiddleware
from fastapi.staticfiles import StaticFiles
from infrastructure import prisma
import view
import api

load_dotenv(os.path.abspath(os.path.join(os.path.dirname(__file__), ".env")))
logging.basicConfig(level=setting.LOG_LEVEL)


def create_app():
    app = FastAPI()

    # <<------ EVENT ------->>
    @app.on_event("startup")
    async def startup():
        await prisma.connect()

    @app.on_event("shutdown")
    async def shutdown():
        await prisma.disconnect()

    # <<------ MOUNT ------->>
    app.mount("/dash", WSGIMiddleware(infrastructure.dash_app.server))
    app.mount("/static", StaticFiles(directory="static"), name="static")

    # <<------ BIND FRONTEND WEB ------->>
    view.bindWebApplication(app)

    # <<------ API HEALTH CHECK ------->>
    @app.get("/health", tags=["health_check"])
    async def healthcheck():
        return {"ok": True}

    # <<------ API BINDING ------->>
    app.include_router(api.getUserRouter())

    logging.info("QuantSmith Application Created")
    return app


app = create_app()


if __name__ == "__main__":
    if setting.MODE == "development":
        uvicorn.run(
            "main:app", host=setting.APP_HOST, port=setting.APP_PORT, reload=True
        )
    else:
        uvicorn.run("main:app", host=setting.APP_HOST, port=setting.APP_PORT)
