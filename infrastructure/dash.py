from dash import html
import dash_bootstrap_components as dbc
import dash


dash_app = dash.Dash(
    __name__, requests_pathname_prefix="/dash/", title="Dashboard Demo"
)

header = dbc.Row(
    dbc.Col(
        [
            html.Div(style={"height": 30}),
            html.H1("Demo", className="text-center"),
        ]
    ),
    className="mb-4",
)

dash_app.layout = dbc.Container(
    [
        header,
    ],
    fluid=True,
    className="bg-light",
)
