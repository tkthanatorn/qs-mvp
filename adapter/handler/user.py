from fastapi import APIRouter, Request
from core.domain import IUserService


class UserHandler:
    serv: IUserService

    def __init__(self, serv: IUserService) -> None:
        self.serv = serv

    def bindUserAPI(self, router: APIRouter):
        @router.post("/", status_code=200)
        async def login(request: Request):
            form = await request.form()
            return {"email": form.get("email"), "password": form.get("password")}
