from prisma import Prisma
from prisma.client import models
from core.port import IUserRepo


class UserRepo(IUserRepo):
    prisma: Prisma

    def __init__(self, prisma: Prisma) -> None:
        super().__init__()
        self.prisma = prisma

    async def create(self, user: models.User):
        try:
            await self.prisma.user.create(
                data={
                    "email": user.email,
                    "password": user.password,
                }
            )
        except Exception as e:
            raise e

    async def getByEmail(self, email: str) -> models.User | None:
        try:
            user = await self.prisma.user.find_first(where={"email": email})
            return user
        except Exception as e:
            raise e
